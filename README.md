# jnk (جنك)

![junk](junk.jpg)

A small utility for quick deployment of a basic container setup to run and confiure OpenVPN services. Intended for development and testing of [menshen](https://0xacab.org/leap/menshen), [bridges](https://0xacab.org/leap/obfsvpn) and OpenVPN gateways.

## Configuration

You can pass an ini-style config file with the `--config` flag. Below you find an example configuration you can copy and paste to use jnk in a local setup.

In the `[images]` section you can override which images are used for each
service (by default it will use the latest in 0xacab registry for each
project).

* The `menshen` block is mandatory, since it's the main port that we will expose.
* The `openvpn` block is optional. If it is enabled, it will run an OpenVPN container (TODO: auto-configure menshen to read vars from the control port).
* The `bridge-x` blocks are optional. It will run as many `obfsvpn` containers as needed for each type configured. Bridges sections need to follow the scheme `[bridge-<number>]`.

You need to bind different ports for each container ( specified in `addr`) if you run them on the same machine ;).

You can let handle menshen the Let's Encrypt certificate, in case you want to deploy the services on a remote machine and access the menshen API via https. To do so, change the `[menshen]` block of the example config below:
```
[menshen]
...
auto-tls = true
server-name = <domain name of the remote machine>
...
```

### Example config
```ini
[images]
menshen =
obfsvpn =
openvpn =

[menshen]
enabled = true
auto-tls = false
from-eip-url = https://api.black.riseup.net:4430/3/config/eip-service.json
allow-gateway-list = true
allow-bridge-list = true
bridges = bridge-1:9090
port = 80

[openvpn]
enabled = false
addr = localhost:1194

[bridge-1]
enabled = true
type = obfs4
gateway = 163.172.211.109:1194
location = amsterdam
addr = localhost:4430

[bridge-2]
enabled = false
type = obfs4-kcp
gateway = 163.172.211.109:1194
location = amsterdam
addr = localhost:4431
```

## Run jnk

First pull the images of the services we want to run:
`./jnk --config your_config_file.ini pull`

Then run the images in containers using:
`./jnk --config your_config_file.ini run`

## Swagger API

If the containers are running you should be able to access the swagger api at
http://localhost/api/swagger/index.html
