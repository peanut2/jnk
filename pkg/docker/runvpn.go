package docker

import (
	"fmt"
	"log"
	"strings"

	"0xacab.org/atanarjuat/jnk/pkg/config"
	"github.com/docker/docker/api/types/network"
	"github.com/spf13/viper"
)

var (
	// Container names will break when we have >1 container running for each type
	ContainerNameVPN     = "openvpn"
	ContainerNameMenshen = "menshen"
	ContainerNameBridge  = "obfsvpn"

	NetworkName = "vpn-net"
)

func RunVPN() {
	cfg := config.NewConfigFromViper()

	net := &network.NetworkingConfig{
		EndpointsConfig: map[string]*network.EndpointSettings{
			NetworkName: {},
		},
	}

	if config.IsOpenVPNEnabled() {
		log.Printf("Running container %s", ContainerNameVPN)
		RunCmdInContainer(cfg.ImageOpenVPN, net, []string{}, &CmdConfig{
			ContainerName: ContainerNameVPN,
			Detach:        true,
			// TODO: make it configurable
			EnvFile:  ".env.host",
			NetAdmin: true,

			// TODO: split in internal and external ports
			PortBindings: []string{
				"1194/udp:1194/udp",
				"9090/tcp:9090/tcp",
			},
		},
			nil)
	}

	if config.IsBridgeEnabled() {
		bridgeNames := getBridgeNames()
		for bridgeName := range bridgeNames {
			isEnabled := viper.GetBool(bridgeName + ".enabled")
			if !isEnabled {
				continue
			}
			log.Printf("starting bridge %s", bridgeName)

			bridgeVPNAddr := viper.GetString(bridgeName + ".addr")
			gateway := viper.GetString(bridgeName + ".gateway")
			location := viper.GetString(bridgeName + ".location")
			parts := strings.Split(bridgeVPNAddr, ":")
			if len(parts) != 2 {
				fmt.Println("parts:", parts)
				panic("malformed addr: " + gateway)
			}
			bridgeIP, bridgePort := parts[0], parts[1]

			log.Printf("Running container %s", bridgeName)
			RunCmdInContainer(cfg.ImageObfsVPN, net, []string{"/opt/obfsvpn/start_obfsvpn.sh"}, &CmdConfig{
				ContainerName: bridgeName,
				Detach:        true,
				Env: []string{
					fmt.Sprintf("CONTROL_PORT=%s", "9090"),
					fmt.Sprintf("OBFSVPN_IP=%s", bridgeIP),
					fmt.Sprintf("OBFSVPN_PORT=%s", bridgePort),
					fmt.Sprintf("OBFSVPN_VPNADDR=%v", gateway),
					fmt.Sprintf("OBFSVPN_LOCATION=%s", location),
					"OBFSVPN_ADDR=0.0.0.0",
					"OBFS4_KEY_FILE=/opt/obfsvpn/obfsvpn-server-test-data/obfs4.json",
				},
				NetAdmin: true,
				ExposedPorts: []string{
					"9090/tcp",
				},
				PortBindings: []string{
					bridgePort + "/tcp:" + bridgePort + "/tcp",
				},
			},
				nil)
		}
	}

	if config.IsMenshenEnabled() {
		// TODO: should get it from config.GetBridgeControlPorts() - iterate thru all enabled bridge sections.
		bridges := viper.GetString("menshen.bridges")

		log.Printf("Running container %s", ContainerNameMenshen)
		RunCmdInContainer(cfg.ImageMenshen, net, []string{}, &CmdConfig{
			ContainerName: ContainerNameMenshen,
			Detach:        true,
			Env: []string{
				fmt.Sprintf("MENSHEN_ALLOW_GATEWAY_LIST=%v", cfg.MenshenAllowGatewayList),
				fmt.Sprintf("MENSHEN_ALLOW_BRIDGE_LIST=%v", cfg.MenshenAllowBridgeList),
				fmt.Sprintf("MENSHEN_AUTO_TLS=%v", cfg.MenshenAutoTLS),
				fmt.Sprintf("MENSHEN_PORT=%v", cfg.MenshenPort),
				fmt.Sprintf("MENSHEN_SERVER_NAME=%v", cfg.MenshenServerName),
				fmt.Sprintf("MENSHEN_FROM_EIP_URL=%s", cfg.MenshenFromEIPURL),
				fmt.Sprintf("MENSHEN_LOCALBRIDGES=%s", bridges),
			},
			NetAdmin: true,
			PortBindings: []string{
				fmt.Sprintf("%d/tcp:%d/tcp", cfg.MenshenPort, cfg.MenshenPort),
			},
		},
			nil)
	}
}

func getBridgeNames() map[string]struct{} {
	bridgeNames := make(map[string]struct{})
	for _, key := range viper.AllKeys() {
		if strings.HasPrefix(key, "bridge") {
			bridgePrefix := strings.Split(key, ".")[0]
			bridgeNames[bridgePrefix] = struct{}{}
		}
	}
	return bridgeNames
}

func StopVPN() {
	StopContainer(ContainerNameVPN, true)
	StopContainer(ContainerNameMenshen, true)

	bridgeNames := getBridgeNames()
	for bridgeName := range bridgeNames {
		isEnabled := viper.GetBool(bridgeName + ".enabled")
		if !isEnabled {
			continue
		}
		StopContainer(bridgeName, true)
	}
}
