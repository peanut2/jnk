// config holds configuration for all the jnk application components
package config

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/spf13/viper"
	"gopkg.in/ini.v1"
)

const (
	defaultMenshenPort      int    = 443
	defaultNoTlsMenshenPort        = 8080
	defaultImageMenshen     string = "registry.0xacab.org/leap/menshen/menshen"
	defaultImageOpenVPN     string = "registry.0xacab.org/leap/openvpn-docker-standalone/openvpn-docker-standalone"
	defaultImageObfsVPN     string = "registry.0xacab.org/leap/obfsvpn:server-main"
)

type service string
type addr string

var (
	menshen service = "menshen"
	openvpn service = "openvpn"
	bridge  service = "bridge"
)

type Config struct {
	ImageMenshen            string
	ImageOpenVPN            string
	ImageObfsVPN            string
	MenshenAllowGatewayList bool
	MenshenAllowBridgeList  bool
	MenshenAutoTLS          bool
	MenshenFromEIPURL       string
	MenshenPort             int
	MenshenServerName       string

	// openvpn is a map with container index as key and addr as value
	openvpn map[int]addr
}

func NewConfig() *Config {
	return &Config{
		ImageMenshen: defaultImageMenshen,
		ImageOpenVPN: defaultImageOpenVPN,
		ImageObfsVPN: defaultImageObfsVPN,
		MenshenPort:  defaultMenshenPort,
		openvpn:      make(map[int]addr),
	}
}

// NewConfigFromViper is intended to be used together with viper, which should be handled
// by the command.
func NewConfigFromViper() *Config {
	cfg := NewConfig()

	if img := viper.GetString("images.menshen"); img != "" {
		cfg.ImageMenshen = img
	}
	if img := viper.GetString("images.openvpn"); img != "" {
		cfg.ImageOpenVPN = img
	}
	if img := viper.GetString("images.obfsvpn"); img != "" {
		cfg.ImageObfsVPN = img
	}

	if port := viper.GetInt("menshen.port"); port != 0 {
		cfg.MenshenPort = port
	}
	if url := viper.GetString("menshen.from-eip-url"); url != "" {
		cfg.MenshenFromEIPURL = url
	}
	if name := viper.GetString("menshen.server-name"); name != "" {
		cfg.MenshenServerName = name
	}
	if autotls := viper.GetBool("menshen.auto-tls"); autotls {
		if !autotls && cfg.MenshenPort == defaultMenshenPort {
			cfg.MenshenPort = defaultNoTlsMenshenPort
		} else {
			cfg.MenshenAutoTLS = autotls
		}
	}
	gwList := viper.GetBool("menshen.allow-gateway-list")
	cfg.MenshenAllowGatewayList = gwList

	brList := viper.GetBool("menshen.allow-bridge-list")
	cfg.MenshenAllowBridgeList = brList

	checkServicesEnabled(cfg, viper.ConfigFileUsed())

	return cfg
}

// sectionsEnabled keeps count of what services are enabled
var sectionsEnabled = make(map[service]bool)

func IsMenshenEnabled() bool {
	return sectionsEnabled[menshen]
}

func IsOpenVPNEnabled() bool {
	return sectionsEnabled[openvpn]
}

func IsBridgeEnabled() bool {
	return sectionsEnabled[bridge]
}

func extractIndex(name, prefix string) (int, error) {
	if name == prefix {
		return 0, nil
	}
	if len(name) < len(prefix)+1 {
		return 0, fmt.Errorf("wrong prefix")
	}
	postfix := name[len(prefix)+1:]
	idx, err := strconv.Atoi(postfix)
	if err != nil {
		return 0, err
	}
	return idx, nil
}

func checkServicesEnabled(cfg *Config, fileName string) {
	_cfg, err := ini.Load(fileName)
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		return
	}

	for _, section := range _cfg.Sections() {
		if len(section.Keys()) == 0 {
			fmt.Printf("Section [%s] is empty\n", section.Name())
		}
		var val bool
		name := service(section.Name())

		if name == menshen {
			val = viper.GetBool(fmt.Sprintf("%s.enabled", name))
			if val {
				sectionsEnabled["menshen"] = true
			}

		}
		if name == openvpn || strings.HasPrefix(string(name), string(openvpn)+"-") {
			val = viper.GetBool(fmt.Sprintf("%s.enabled", name))
			if val {
				idx, err := extractIndex(string(name), string(openvpn))
				if err != nil {
					log.Fatal("wrong section identifier: " + err.Error())
				}
				addrStr := viper.GetString(fmt.Sprintf("%s.addr", name))
				if addrStr == "" {
					log.Fatal("missing addr for: " + name)
				}
				cfg.openvpn[idx] = addr(addrStr)
				sectionsEnabled[openvpn] = true
			}
		}
		if name == bridge || strings.HasPrefix(string(name), string(bridge)+"-") {
			val = viper.GetBool(fmt.Sprintf("%s.enabled", name))
			if val {
				_, err := extractIndex(string(name), string(bridge))
				if err != nil {
					log.Fatal("wrong section identifier: " + err.Error())
				}
				addrStr := viper.GetString(fmt.Sprintf("%s.addr", name))
				if addrStr == "" {
					log.Fatal("missing addr for: " + name)
				}
				sectionsEnabled[bridge] = true
			}
		}
	}
	// TODO: we could do an extra check to see if the bridges are in correlative order.
}
